import gramfuzz
import sys

def fuzzer_simple():
    fuzzer = gramfuzz.GramFuzzer()
    fuzzer.load_grammar("SeQreL_grammar.py")
    queries = fuzzer.gen(cat="query", num=20, max_recursion=30)
    print(str.encode("\n").join(queries).decode())


#############################################

fuzzer_simple()