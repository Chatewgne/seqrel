# SeQreL


Ce projet rasssemble les différents composants du projet SeQreL.

SeQreL est un langage de programmation équivalent à un sous-ensemble du SQL-92 et sûr pour un point d'injection n'importe où dans la requête, plusieurs points d'injection dans certains cas.


L'approche formelle, les détails de conception du langage, ainsi que la méthode de placement des points d'injection sont détaillés dans 
```Rapport-SeQreL.pdf```.

## Banque de requêtes

Le dossier ```queries``` contient des requêtes SQL et leur équivalent en SeQreL.

Chaque fichier ```queries/seqrel/queryX``` contient la traduction en SeQreL de la requête contenue dans ```queries/sql/queryX```.


## TransQpilor

Ce dossier contient le projet de transcompilateur qui permet de traduire automatiquement une requête SeQreL en SQL. L'idée est de sécuriser les architectures existantes en implémentant les applications en SeQreL de sorte à contrôler les injections au moment où l'utilisateur interagit avec l'application, puis les traduire en SQL pour l'exécution.

![Architecture sécurisée avec un transcompilateur SeQreL->SQL](readme-img/architecture.jpg)

### Compilation

```cd TransQpilor```

(Si nécessaire ```mkdir build``` et ```mkdir bin```.)

```make```

### Utilisation

```cd TransQpilor```

```./bin/compiler < ./input/input_file```

### Modification

Dans ```TransQpilor/src/seqrel.l``` : Définition des différents tokens. A modifier pour ajouter un mot-clé ou une balise. 

Dans ```TransQpilor/src/instruction.c``` : Contient des fonctions pour constuire la requête de sortie au fur et à mesure du parsing (```add_to_query()```) et des méthodes pour gérer les chaînes de caractères (concaténation).

Dans ```TransQpilor/src/ast.c``` : Implémentation de l'arbre binaire qui permet d'extraire la structure de la clause WHERE. Voir page 32 du [rapport](https://gitlab.com/Chatewgne/seqrel/-/blob/master/Rapport-SeQreL.pdf).

Dans ```TransQpilor/src/stack.c``` : Implémentation de la pile qui permet de changer l'ordre des mots-clés. Voir page 35 du [rapport](https://gitlab.com/Chatewgne/seqrel/-/blob/master/Rapport-SeQreL.pdf).

Dans ```TransQpilor/src/rule.y``` : Le parser qui décrit les règles de grammaire et les actions qui correspondent dans la construction de la requête de sortie. On définit des structures globables modifiées en appellant les fonctions des autres fichiers. A modifier pour ajouter une règle de grammaire et les actions qui correspondent.

En cas d'ajout d'une fonction dans le code C penser à modifier le header correspondant dans ```TransQpilor/header/```.

## Fuzzer

Le fuzzer permet de générer des requêtes SeQreL aléatoires. Il faut s'assurer de développer le fuzzer en parallèle du transcompilateur, toute règle ajoutée à la grammaire du transcompilateur doit aussi être ajoutée à celle du fuzzer. Ainsi toute requête générée par le fuzzer doit être compilable par le transcompilateur (pas d'erreur de syntaxe entre les deux outils).

### Utilisation

```cd Fuzzer```

```python3 -m pip install gramfuzz```

```python3 SeQreL_fuzzer.py```

### Modification

Dans ```Fuzzer/SeQreL_grammar.py``` : Les règles de grammaires décrites dans la syntaxe gramfuzz. A modifier pour ajouter des règles.

Dans ```Fuzzer/SeQreL_fuzzer.py``` : Instanciation et lancement du fuzzer. Le paramètre ```num``` correspond au nombre de requêtes générées, le paramètre ```max_recursion``` impacte la taille moyenne des requêtes.


## La grammaire du SeQreL

Voici la grammaire complète du SeQreL (sémantiquement équivalente à la sous-grammaire SQL-92 disponible en annexe D du [rapport](https://gitlab.com/Chatewgne/seqrel/-/blob/master/Rapport-SeQreL.pdf)).

Les règles surlignées en bleu ne sont pas implémentées dans le transcompilateur (sous-requêtes).

![](readme-img/grammaire.png)
