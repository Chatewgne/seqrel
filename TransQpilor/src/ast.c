#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "../header/ast.h"

//Define binary tree
typedef struct node {
    const char * value;
    struct node *parent;
    struct node *left;
    struct node *right;
} Node;

//Init empty trees
Node * Tree = NULL;
Node * Current = NULL ;

//Variables for leaf size and content
int data_size ;
char * str ;

//Init new tree node with value and parent
Node* new_node(const char * val, Node * parent) 
{ 
    Node* node = (Node*)malloc(sizeof(Node)+sizeof(val)); 
    node->parent = parent ;
    node->value = val;  
    node->left = NULL; 
    node->right = NULL; 
    return (node); 
}

//Add tree node (AND, OR operators)
void add_node(const char * val){
    Node* node = new_node(val, Current);
    if(Tree==NULL){
        Tree = node ;
        Current = node ;
        data_size = strlen(val);
    } else {
        //add left son
        if (Current->left == NULL){    
            Current->left = node;
            Current = node ;
            data_size += strlen(val) + 4; //TODO verifier
        //add right son 
        } else if (Current->right == NULL ) {
            Current->right = node;
            Current = node;
            data_size += strlen(val) + 4 ; //TODO verifier 
        } else {
            printf("Current node is full\n");
        }
    }
}

//Add tree leaves (predicates)
void add_leaf(const char * val){
    Node* node = new_node(val, Current);
    if(Tree==NULL){
        Tree = node ;
        Current = node ;
        data_size = strlen(val);
    //add left leaf
    } else if (Current->left == NULL){
        Current->left = node;
        data_size += strlen(val) + 2;
    //add right leaf and exit this node
    } else if (Current->right == NULL ) {
        Current->right = node;
        data_size += strlen(val) + 2;
        while (Current != Tree && Current->right!=NULL){
            Current = Current->parent; 
        }
    }
}

//Free memory
void free_tree_recurs(Node * node){
       if (node != NULL) {
        free_tree_recurs(node->right);
        free_tree_recurs(node->left);
        free(node);
     }
}

//Free memory
void free_tree(){
    free_tree_recurs(Tree);
    Tree = NULL;
    Current = NULL;
}

//Debug method
void print_tree(Node * node){
    if (node!=NULL){
       if ((node->left==NULL)&&(node->right==NULL)){
            printf("Leaf %s\n",node->value);
       } else {
        printf("Node %s\n",node->value);
        print_tree(node->right);
        print_tree(node->left);
       }
    }
}

//Recursively turn tree into SQL predicate syntax string
static char * flatten(Node * node){
    if (node != NULL){
        if ((node->left==NULL)&&(node->right==NULL)){
            strcat(str,node->value);
           // printf("str = %s\n",str);
        } else {   
            strcat(str,"( ");
            flatten(node->left);
            strcat(str," ");
            strcat(str,node->value);
           // printf("str = %s\n",str);
            strcat(str," ");
            flatten(node->right);
            strcat(str," )");
        }
        return str;
    } else {
        yyerror("Cannot flatten empty tree\n");
        return "NULL";
    }
}

//Call recursive function to turn tree into SQL predicate syntax string
const char * tree_to_string(){
    str = malloc(data_size+1);
    *str = '\0';
    if (Tree!=NULL){
       return flatten(Tree);
    } else {
        yyerror("Cannot flatten empty tree\n");
    }   
    return str;
}