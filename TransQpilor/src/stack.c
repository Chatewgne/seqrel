#include "../header/stack.h"
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h>

//Define stack structure
struct Stack { 
    int top; 
    char * * array; 
}; 

//Init empty stack
struct Stack* createStack() 
{ 
    struct Stack* stack = (struct Stack*)malloc(sizeof(struct Stack)); 
    stack->top = -1; 
    stack->array = (char * *)malloc(STACK_CAPACITY * sizeof(char *)); 
    return stack; 
} 

int isFull(struct Stack* stack) 
{ 
    return stack->top == STACK_CAPACITY; 
} 
  
int isEmpty(struct Stack* stack) 
{ 
    return stack->top == -1; 
} 
  
//Add element on top of stack
void push(struct Stack* stack, char * suffix) 
{ 
    if (isFull(stack))
        perror("Error : Stack is full\n");
    stack->array[++stack->top] = suffix; 
    //printf("@%p : %s pushed to stack\n", suffix, suffix); 
} 
  
//Remove element from top of stack
char * pop(struct Stack* stack) 
{ 
    if (isEmpty(stack)) 
        perror("Error : Cannot pop from empty stack\n"); 
    //printf("Popin' \n"); 
    return stack->array[stack->top--]; 
} 
  
// Return top element without removing it 
int peek(struct Stack* stack) 
{ 
    if (isEmpty(stack)) 
        perror("Error : Cannot peek on empty stack\n");  
    return stack->array[stack->top]; 
} 
  
// Test stack with capacity = 3
int stack_test() 
{ 
    
    printf("stack test\n");
    struct Stack* stack = createStack(); 
    char * various = "Hellooo";
    char * various1 = "Hi";
    char * various2 = "Heya";
    push(stack, various1); 
    push(stack, various2); 
    push(stack, various); 
    push(stack, various2); 
    //should fail
    //push(stack, various2); 

    char * poped = pop(stack);
    printf("%p : %s popped from stack\n",poped,poped); 
    poped = pop(stack);
    printf("%p : %s popped from stack\n",poped,poped); 
    poped = pop(stack);
    printf("%p : %s popped from stack\n",poped,poped); 
    poped = pop(stack);
    printf("%p : %s popped from stack\n",poped,poped); 

    //should fail
    //poped = pop(stack);
  
    return 0; 
} 