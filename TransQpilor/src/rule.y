%{
  #include <math.h>
	#include <stdio.h>
	#include <string.h>
  #include "../header/instruction.h"
  #include "../header/ast.h"
  #include "../header/stack.h"

  int yylex(void);
	void yyerror(char*);

  //Buffer in which to build the output query
  char * query_buffer;
  //Memorizes query elements, to append to the output query later
  char * suffix_buffer;
  //Buffer used in building IN/NOT_IN predicates
  char * pred_buffer;
  //Stack used in keywords re-ordering
  struct Stack* stack;


	int yydebug = 1;
	extern int yylineno;

	int constant = 0;
	char* type;	

%}

%union {
	char* str;
	int nb;
}

%token tSELECT tFROM tWHERE tSEP tTAGO1 tTAGC1 tSELECTALL tORDERBY tASC tDESC tUNION tGROUPBY tDOT
%token tALL_FROM tTAGO3 tTAGC3 tTAGO4 tTAGC4 tTAGO5 tTAGC5 tTAGO6 tTAGC6 tEQ tDIFF tSUPEQ tINFEQ tSUP tINF tSEP2 tAND tOR
%token tIN tNOTIN tSUBIN tSUBNOTIN tAS tASREF tDISTINCT tSUM tAVG tMAX tMIN tCOUNT tAP
%token <str> tID tNB
%type <str> comp_op column_ref value column_location renamed_set_function set_quantifier column_name sort_key ordering_spec sort_spec table_ref set_function_type set_function


%%

/*Init stack*/
start: {stack = createStack();} select_or_order_query ;

select_or_order_query: select_query 
                     | tORDERBY { //Append ORDER BY to suffix buffer
                                  suffix_buffer = new_buffer(); 
                                  concat_buff("ORDER BY", suffix_buffer, SPACE);
                                }
                     ordered_select_query tTAGC1 {
                                                    //Pop "ORDER BY" keyword from stack and append to output query
                                                    add_to_query(pop(stack),SPACE);
                                                 };

ordered_select_query: sort_list { //Push content of suffix buffer ("ORDER BY colum1,column2") to top of stack
                                    push(stack,suffix_buffer); 
                                }  tSEP select_query;

sort_list: tTAGO2 sort_spec {        
                                     //Append column values to suffix buffer (including comas)
                                     const char * str = ", ";
                                     strncat($2,str,2);
                                     concat_buff($2,suffix_buffer, NOSPACE);
                          } tSEP sort_list tSUP
          | tTAGO1 sort_spec { 
                              //Append column values to suffix buffer (including comas)
                              concat_buff($2,suffix_buffer, NOSPACE);
                              } tTAGC1 ;

sort_spec: sort_key ordering_spec {
                                    //Return sorting specifications to upper rule
                                    char c=' ';
                                    strncat($1,&c,1);
                                    strcat($1,$2);
                                    $$=$1;
                                  };

/*Return keyword to upper rule*/
ordering_spec: tASC  {$$="ASC";} 
             | tDESC {$$="DESC";} ;

/*Return column name to upper rule */
sort_key: column_name {$$=$1;} ;

select_query: simple_table 
              | tUNION {
                        //Push "UNION" keyword to top of stack
                        suffix_buffer = new_buffer();
                        concat_buff("UNION", suffix_buffer, NOSPACE);
                        push(stack,suffix_buffer);
                       } select_query tSEP2
                       {
                         //Pop "UNION" keyword from stack and append to output query
                         add_to_query(pop(stack),SPACE);
                       }
                        select_query tTAGC5
              | tGROUPBY { //Append GROUP BY to suffix buffer
                           suffix_buffer = new_buffer(); 
                           concat_buff("GROUP BY", suffix_buffer, SPACE);}
                grouped_simple_table tTAGC6 { //Pop "GROUP BY column1,column2" from stack and append to output query
                                              add_to_query(pop(stack),SPACE);
                                            } ;

grouped_simple_table: grouping_column_list {  //Push content of suffix buffer ("GROUP BY colum1,column2") to top of stack
                                              push(stack,suffix_buffer); 
                                            } tSEP simple_table;

grouping_column_list: tTAGO2 column_name {    
                                          //Append column values to suffix buffer (including comas)
                                          const char * str = ", ";
                                          strncat($2,str,2);
                                          concat_buff($2,suffix_buffer, NOSPACE);
                                         } tSEP grouping_column_list tTAGC2
          | tTAGO1 column_name {  //Append column values to suffix buffer (including comas)
                                  concat_buff($2,suffix_buffer, NOSPACE);
                              } tTAGC1 ;


simple_table: query_all_from 
            | query_list
            | query_simple 
            ;

/*Append "SELECT *" to output query*/
query_all_from: tSELECTALL {add_to_query("SELECT *", SPACE);} table_expr ;

/*Append "SELECT column1" to output query*/
query_simple: tSELECT {add_to_query("SELECT", SPACE);} tTAGO1 column_ref {add_to_query($4, SPACE);} tTAGC1 table_expr ;

/*Append "SELECT" to output query*/
query_list:  tSELECT {add_to_query("SELECT", SPACE);} select_list  table_expr ;

table_expr: all_from_table_expr 
          | from_where_table_expr
          ;
from_where_table_expr: from_clause where_clause ;

/*Append "FROM" to output query*/
all_from_table_expr: tALL_FROM {add_to_query("FROM", SPACE);} table_ref_list ;  

/*Append "FROM" to output query*/
from_clause: tFROM {add_to_query("FROM", SPACE);} table_ref_list;

/*Append table names list to query output*/
table_ref_list: tTAGO1 table_ref { add_to_query($2,SPACE); } tTAGC1
           | tTAGO2 table_ref_in_group tSEP table_ref_list tTAGC2 ;

where_clause: tWHERE { //Append "WHERE" to output query
                       add_to_query("WHERE", SPACE);
                      } search_cond { 
                                      //Append re-ordered WHERE predicate to output query and free tree
                                      add_to_query(tree_to_string(),SPACE); 
                                      free_tree(); 
                                    } ; 

search_cond: and_clause
            | or_clause
            | predicate 
            ;

/*Add AND node to predicate tree*/
and_clause: tAND {add_node("AND");} search_conds tTAGC1 ;

/*Add OR node to predicate tree*/
or_clause: tOR {add_node("OR");} search_conds tTAGC1 ;

search_conds: search_cond tSEP search_cond ;

comp_predicate: tTAGO3 predicate_id tTAGC3 
         | tTAGO4 predicate_nb tTAGC4
         ;  

predicate: comp_predicate
         | in_predicate 
         | not_in_predicate ;

in_predicate: tIN column_location tSEP {
                                        //Append IN keyword and first argument (column location) to predicate buffer
                                        char * in = " IN (\0";
                                        pred_buffer=new_buffer();
                                        strncat($2,in,5);
                                        concat_buff($2,pred_buffer, NOSPACE);
                                       }
                                        value_list tTAGC1 { 
                                                            //Add content of predicate buffer ("IN 'value1','value2'") as leaf in predicate tree
                                                            char * par = ")\0";
                                                            concat_buff(par,pred_buffer,NOSPACE);
                                                            add_leaf(pred_buffer);
                                                          };

not_in_predicate: tNOTIN column_location tSEP {
                                        //Append NOT IN keyword and first argument (column location)  to predicate buffer
                                        char * in = " NOT IN (\0";
                                        pred_buffer=new_buffer();
                                        strncat($2,in,9);
                                        concat_buff($2,pred_buffer, NOSPACE);
                                       }
                                        value_list tTAGC1 { //Add content of predicate buffer ("NOT IN 'value1','value2'") as leaf in predicate tree
                                                            char * par = ")\0";
                                                            concat_buff(par,pred_buffer,NOSPACE);
                                                            add_leaf(pred_buffer);
                                                          };

value: tAP tID tAP {
                    //Return value (including quotes) to upper rule
                    char * ap = "'\0";
                    $$=concat3(ap,$2,ap,NOSPACE);
                   };

value_list: tTAGO1 value tTAGC1  { 
                                    //Append value to suffix buffer
                                    concat_buff($2,pred_buffer, NOSPACE);
                                  }
          | tTAGO2 value {  
                            //Append values list to suffix buffer (including comas)
                            const char * str = ", ";
                            strncat($2,str,2);
                            concat_buff($2,pred_buffer, NOSPACE);
                          } tSEP value_list tTAGC2 ;

predicate_id: column_name comp_op tID {
                                //Add comparison predicate ("column1 = value1") as leaf in predicate tree
                                //ID can be either column name or string value  
                                add_leaf(concat3($1,$2,$3,SPACE)); 
                              } ;

predicate_nb: tID comp_op tNB { 
                                //Add comparison predicate ("column1 = 42") as leaf in predicate tree
                                add_leaf(concat3($1,$2,$3,SPACE));
                              } ;

/*Return operator to upper rule*/
comp_op: tEQ {$$ = "="; }
       | tDIFF {$$ = "!=" ; }
       | tSUPEQ {$$ = ">="; }
       | tINFEQ {$$ = "<="; }
       | tSUP {$$ = ">"; }
       | tINF {$$ = "<"; }
       ;

select_list: tTAGO1 column_ref {
                                  //Append column names list to output query (including comas)
                                  add_to_query($2, SPACE);
                                } tTAGC1
           | tTAGO2 column_ref {
                                     //Append column names list to output query (including comas)
                                     char c=',';
                                     strncat($2,&c,1);
                                     add_to_query($2, NOSPACE);
                                } tSEP select_list tTAGC2 ;

/*Return colum name value to upper rule*/
column_name: tID {$$=$1;} ;

/*Return column reference to upper rule*/
column_ref: column_location {$$=$1;}  
          | set_function {$$=$1;} 
          | renamed_set_function {$$ = $1;} ;

renamed_set_function: tAS column_location tSEP set_function tTAGC1 {
                                                                    //Build renaming string ("function AS column_name") and return to upper rule
                                                                    char * as = "AS\0";
                                                                    $$ = concat3($4,as,$2,SPACE) ;
                                                                   }

set_function: set_function_type tTAGO3 column_name tTAGC3 tTAGC1 {
                                                                    //Build renaming function ("MIN(column)") and return to upper rule
                                                                    char * p = ")\0";
                                                                    $$ = concat3($1,$3,p,NOSPACE);
                                                                  } ;
            | set_quantifier tTAGO3 column_name tTAGC3 tTAGC1 {
                                                                    //Build renaming function ("DISTINCT(column)") and return to upper rule
                                                                    char * p = ")\0";
                                                                    $$ = concat3($1,$3,p,NOSPACE);
                                                                  } ;
            | set_function_type set_quantifier tTAGO3 column_name tTAGC3 tTAGC1 tTAGC1 {
                                                                                //Build renaming function ("COUNT(DISTINCT((column))") and return to upper rule
                                                                                 char * end = "))\0";
                                                                                 strncat($4,end,2);
                                                                                 $$ = concat3($1,$2,$4,NOSPACE);
                                                                                } ;

/*Return function keyword to upper rule*/
set_function_type: tAVG {$$="AVG(";}
                 | tMIN {$$="MIN(";}
                 | tMAX {$$="MAX(";}
                 | tCOUNT {$$="COUNT(";}
                 | tSUM {$$="SUM(";}
                 ;

/*Return function keyword to upper rule*/
set_quantifier: tDISTINCT {$$="DISTINCT(";} ;

/*Return column location (column_name or table.column_name) to upper rule*/
column_location: column_name {$$=$1;}
          | table_ref tDOT column_name {
                                          char * d = ".\0";
                                          $$ = concat3($1,d,$3,NOSPACE);
          };

/*Return table name value to upper rule*/
table_ref: tID {$$=$1;} ;

table_ref_in_group: tID {   //Append table values to output query (including comas)
                            char c=',';
                            strncat($1,&c,1);
                            add_to_query($1, NOSPACE);
                         }; 

tTAGO2: tINF;
tTAGC2: tSUP;

%%

void yyerror(char* s) {
	fprintf(stderr, "%s, at %d, look ahead : %d\n", s, yylineno, yychar);
	//exit(-1);
}

int main() {
  //Init empty query buffer
	init();
  //Parsing
	yyparse();
  //Display output query
	print_query();
  //Free memory
  free_tree();
}