#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "../header/instruction.h"

char query[QUERY_SIZE];
int i;

//Init empty output query
void init(void) {
	query[0]='\0';
    i=0;
}

//Init new empty buffer with max size
char * new_buffer(){
    return (char *)malloc(BUFFER_SIZE * sizeof(char));
}

//Display output query
void print_query(void) {
		printf("%s\n",query);
}

//Append string to output query buffer 
void add_to_query(const char *str, int space){
    int length= strlen(str);
    if ((i+length+1) > QUERY_SIZE) {
        perror("Error : Query too big : %d\n");
    }
    else {
        strncpy(&query[i],str,(QUERY_SIZE-i));
        i+=length;
        //Optionally append space after string
        if (space){
            query[i]=' ';
            i++;
        }
    }
}

//Concat 3 strings into one with or without spacing
char * concat3(const char *str1, const char *str2, const char *str3, int space){
    char * str = (char*)malloc(strlen(str1)+strlen(str2)+strlen(str3)+2);
    char c=' ';
    strncat(str,str1,strlen(str1));
    if (space){
        strncat(str,&c,1);
    }
    strncat(str,str2,strlen(str2));
    if (space){
        strncat(str,&c,1);
    }
    strncat(str,str3,strlen(str3));
    return str;
}

//Concat 2 strings into one with or without spacing
char * concat2(const char *str1, const char *str2, int space){
    char * str = (char*)malloc(strlen(str1)+strlen(str2)+1);
    char c=' ';
    strncat(str,str1,strlen(str1));
    if (space){
        strncat(str,&c,1);
    }
    strncat(str,str2,strlen(str2));
    if (space){
        strncat(str,&c,1);
    }
    return str;
}

//Concat 3 strings into one without spacing and add to query buffer
void add_3_to_query(const char *str1, const char *str2, const char *str3, int space){
    char * str = concat3(str1,str2,str3,NOSPACE);
    add_to_query(str, space);
}

//Append string to an existing buffer, with or without spacings
void concat_buff(const char *str, char * buff, int space){
    int length= strlen(str) + strlen(buff);
    if ((length+1) > BUFFER_SIZE) {
        perror("Error : Buffer is full\n");
    }
    else {
        //printf("Adding %s to buff\n",str);
        strcat(buff,str);
        if (space){
            char c = ' ';
            strncat(buff,&c,1);
        }
    }
}


