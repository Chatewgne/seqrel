//Define stack size
#define STACK_CAPACITY 50

struct Stack* createStack();
int isFull(struct Stack* stack);
int isEmpty(struct Stack* stack);
void push(struct Stack* stack, char * suffix);
char * pop(struct Stack* stack);
int peek(struct Stack* stack);
int stack_test();
